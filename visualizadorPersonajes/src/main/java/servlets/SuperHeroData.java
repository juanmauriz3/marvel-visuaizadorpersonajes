package servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import classes.UrlGenerator;
import classes.CharacterDataManager;
import classes.HTMLGenerator;
import datatypes.CharacterData;

/**
 * Servlet implementation class SuperHeroData
 */
public class SuperHeroData extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = null;
		CharacterData data = null;
		Integer id = 0;
		try {
			out = response.getWriter();
			if(request.getParameter("id") != null) {
				id = Integer.parseInt(request.getParameter("id"));
				String url = UrlGenerator.getURl(id);
				data = CharacterDataManager.getData(url);
			}
			String html = HTMLGenerator.generateHTML(id, data);
			out.println(html);
		}catch (FileNotFoundException e) {
			String html = "No se encontro el personaje" + HTMLGenerator.generateHTML(id, null);
			out.println(html);
		} catch (Exception e) {
			out.println("Error: " + e.getStackTrace());
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
