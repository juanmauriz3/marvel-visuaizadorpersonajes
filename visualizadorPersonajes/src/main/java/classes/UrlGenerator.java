package classes;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import datatypes.UserCredentials;

public class UrlGenerator {
	/*
	 * Returns an URL for fetching a single character eg
	 * https://gateway.marvel.com/v1/public/characters/<character
	 * id>?ts=<timestamp>&apikey=<public key>&hash=<digest md5(timestamp + private
	 * key + public key)>
	 */
	static public String getURl(Integer characterId) throws NoSuchAlgorithmException, IOException {
		String timestamp = "1";
		UserCredentials credentials = new UserCredentials(timestamp);
		String characterIdStr = characterId.toString();
		timestamp = "1";
		String url = "https://gateway.marvel.com/v1/public/characters/" + characterIdStr + "?ts=" + timestamp
				+ "&apikey=" + credentials.getPublicKey() + "&hash=" + credentials.getKeyDigest();
		return url;
	}
}
