package classes;

import datatypes.CharacterData;

public class HTMLGenerator {

	private static String generateInput(String labelName, String name, String value, boolean isDisable,
			boolean isMultiline) {
		String disableText = "";
		if (isDisable) {
			disableText = "disabled";
		}
		if (isMultiline) {
			return "<label style=\"display:flex;flex-direction:column;\"> " + labelName
					+ " <textarea cols=\"40\" rows=\"5\" type=\"text\" name=\"" + name + "\" id=\"" + name + "\" "
					+ disableText + ">" + value + "</textarea></label>";
		}
		return "<label style=\"display:flex;flex-direction:column;\"> " + labelName + " <input type=\"text\" value=\""
				+ value + "\" name=\"" + name + "\" id=\"" + name + "\" " + disableText + "</input></label>";
	}

	static public String generateHTML(Integer id, CharacterData data) {
		String name = "";
		String description = "";
		String amountOfComics = "";
		if (data != null) {
			name = data.getName();
			description = data.getDescription();
			amountOfComics = data.getAmountOfComics();
		}
		String firstPartPage = "<!DOCTYPE html>\n" + "				<html>\n" + "		<head>\n"
				+ "		<meta charset=\"UTF-8\">\n" + "		<title>Visualizador personajes | Marvel</title>\n"
				+ "		</head>\n" + "		<body>\n"
				+ "			<form style=\"display:flex;flex-direction:column;width:500px;\" method=get action=\"/visualizadorPersonajes/MarvelHeroes\">";

		String lastPartPage = "<input type=\"submit\" value=\"SEND QUERY\" />\n" + "		</label>\n" + "	</form>\n"
				+ "</body>\n" + "</html>";
		return firstPartPage + generateInput("Name", "name", name, true, true)
				+ generateInput("Description", "description", description, true, true)
				+ generateInput("# Comics", "#comics", amountOfComics, true, false)
				+ generateInput("ID", "id", id.toString(), false, false) + lastPartPage;
	}

}
