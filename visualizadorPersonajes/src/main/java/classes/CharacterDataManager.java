package classes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import datatypes.CharacterData;

public class CharacterDataManager {

	static private String fetchData(String url) throws IOException {
		URL apiURL = new URL(url);
		URLConnection yc = apiURL.openConnection();
		BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
		String res = "";
		String inputLine;
		while ((inputLine = in.readLine()) != null)
			res += inputLine;
		in.close();

		return res;

	}

	// TODO: hackish, total worse practise, this should use a JSON parser
	static private String getName(String data) {
		Pattern pattern = Pattern.compile("\"name\":\"(.+?)\"");
		Matcher matcher = pattern.matcher(data);
		matcher.find();
		return matcher.group(1);
	}

	// TODO: hackish, total worse practise, this should use a JSON parser
	static private String getDescription(String data) {
		Pattern pattern = Pattern.compile("\"description\":\"(.+?)\"");
		Matcher matcher = pattern.matcher(data);
		matcher.find();
		return matcher.group(1);
	}

	// TODO: hackish, total worse practise, this should use a JSON parser
	static private String getAmountOfComics(String data) {
		Pattern pattern = Pattern.compile("\"available\":(.+?),");
		Matcher matcher = pattern.matcher(data);
		matcher.find();
		return matcher.group(1);
	}
	


	static public CharacterData getData(String url) throws IOException {
		String rawData = fetchData(url);
		CharacterData res = new CharacterData(getName(rawData), getDescription(rawData), getAmountOfComics(rawData));
		return res;
		
	}
}
