package datatypes;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserCredentials {
	private String publickey;
	private String keyDigest;
	
	private String getKey(String keyName) throws IOException {
		Path currentRelativePath = Paths.get("");
		String s = currentRelativePath.toAbsolutePath().toString();
		try {
		Path filePath = Path.of(s + "/keys/keys");
		String content = Files.readString(filePath);
		Pattern pattern = Pattern.compile(keyName + ":(.+)");
	    Matcher matcher = pattern.matcher(content);
	    matcher.find();
	    return matcher.group(1);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	// This function code was copied from https://www.geeksforgeeks.org/md5-hash-in-java/
	private String getMD5(String input) {
		try {
            // Static getInstance method is called with hashing MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
  
            // digest() method is called to calculate message digest
            //  of an input digest() return array of byte
            byte[] messageDigest = md.digest(input.getBytes());
  
            // Convert byte array into signum representation
            BigInteger no = new BigInteger(1, messageDigest);
  
            // Convert message digest into hex value
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } 
        // For specifying wrong message digest algorithms
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
	}
	
	public UserCredentials(String timestamp) throws IOException {
		this.publickey = getKey("public_key");
		String privateKey = getKey("private_key");
		this.keyDigest = getMD5(timestamp + privateKey + this.publickey);
	}
	
	public String getPublicKey() {
		return this.publickey;
	}
	
	public String getKeyDigest() {
		return this.keyDigest;
	}
}
