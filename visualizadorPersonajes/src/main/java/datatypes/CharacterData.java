package datatypes;

public class CharacterData {
	
	private String name;
	private String description;
	private String amountOfComics;
	
	public CharacterData(String name, String description, String amountOfComics) {
		this.name = name;
		this.description = description;
		this.amountOfComics = amountOfComics;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public String getAmountOfComics() {
		return this.amountOfComics;
	}

}
